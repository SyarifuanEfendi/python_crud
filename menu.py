
import os

print("-------- CRUD PYTHON ----------")
print("1. Insert Data")
print("2. Tampilkan Data")
print("3. Update Data")
print("4. Hapus Data")
print("5. Cari Data")
print("0. Keluar")
print("-------- END -----------")

menu = input("Pilih menu> ")

os.system("clear")

if menu == "1":
    from insert import *
elif menu == "2":
    from show import *
elif menu == "3":
    from update import *
elif menu == "4":
    from delete import *
elif menu == "5":
    from search import *
elif menu == "0":
    exit()
else:
    print("Menu Salah!!!")