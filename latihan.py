list = ['laptop', 'handphone', 'speaker']
tuple = ('charger', 'kipas')

a = 10
b = 5

print(list)
print(tuple)

print("hasil dari ",a," + ",b," = ",a+b)
print("hasil dari ",a," - ",b," = ",a-b)
print("hasil dari ",a," / ",b," = ",a/b)
print("hasil dari ",a," * ",b," = ",a*b)

# nilai = int(input("Masukkan Nilai : "))
nilai = 90

if nilai >=90:
    print("Predikat A")
elif nilai >=80:
    print("Predikat B")
elif nilai >=70:
    print("Predikat C")
elif nilai >=60:
    print("Predikat D")
else:
    print("Predikat E")

# cek = input("Masukkan list yang dicari : ")
cek = "laptop"

if (cek in list):
    print("list yang dicari tersedia")
else:
    print("list yang dicari tidak tersedia")

for i, data in enumerate(list):
    print(i, data)
print("end enum")

for ganjil in range(1, 12, 2):
    print(ganjil)
print("end ganjil")

for genap in range(2, 12, 2):
    print(genap)
print("end genap")

for i in range(10, 20):
    if i == 12:
        continue
    print(i)
print("end continue")

for data in list:
    print(data)
else:
    print("tidak ada lagi data yang tersedia")

i=0
while i<=5:
    print(i)
    i+=1
a=0
while a < len(list):
    print(list[a])
    a += 1

kata_unik = {"coba", "coba2" ,"coba3", "coba"}
print(kata_unik)