import mysql.connector

db = mysql.connector.connect(
    host="127.0.0.1",
    user="root",
    password="",
    database="python_crud"
)

# if db.is_connected():
#     print("Database Connected")
# else:
#     print("Database Notconnected")

cursor = db.cursor()

cursor.execute("CREATE DATABASE IF NOT EXISTS python_crud")

sql = """CREATE TABLE IF NOT EXISTS customers (
    customer_id int auto_increment primary key,
    name varchar(255),
    addreas varchar(255)
)"""
cursor.execute(sql)