from itertools import count
from connect import db, cursor

id = input("Masukkan id yang ingin di hapus : ")
cek = "SELECT * FROM customers where customer_id=%s"
isi = (id,)
cursor.execute(cek,isi)
results = cursor.fetchall()

if cursor.rowcount > 0:

    sql = "DELETE FROM customers WHERE customer_id=%s"
    val = (id, )
    cursor.execute(sql, val)
    db.commit()
    print("{} data dihapus".format(cursor.rowcount))

else:
    print("ID tidak ada")
